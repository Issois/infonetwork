
using System;
using System.IO;
using System.Threading;// Semaphore
using System.Collections.Generic;// Dictionary, Queue

using IN=Geraet.InfoNetwork;

namespace Geraet{

/// <summary>
/// The central class. All functionality will be provided via the static methods of this class.
/// </summary>
/// <remarks>
/// Needs to be initialized with <see cref="Initialize"/> before it can be used.<br/>
/// Multithreaded pitfalls:<br/>
/// Setup and configuration (adding, removing, subscribing etc.) is not thread safe.<br/>
/// This should happen on one thread only when no other thread is using the network.<br/>
/// Reading and writing (Broadcast, NextMessage) is thread safe.<br/>
/// </remarks>
public static class InfoNetwork{

/*
* ██╗███╗   ██╗████████╗███████╗██████╗ ███████╗ █████╗  ██████╗███████╗███████╗
* ██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗██╔════╝██╔══██╗██╔════╝██╔════╝██╔════╝
* ██║██╔██╗ ██║   ██║   █████╗  ██████╔╝█████╗  ███████║██║     █████╗  ███████╗
* ██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗██╔══╝  ██╔══██║██║     ██╔══╝  ╚════██║
* ██║██║ ╚████║   ██║   ███████╗██║  ██║██║     ██║  ██║╚██████╗███████╗███████║
* ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝ ╚═════╝╚══════╝╚══════╝
*/
	private interface IReceiver:IDisposable{
		void WriteToBuffer(string message);
		void AbortWaitOnEmpty();
		string NextMessage(CancellationToken token);
		void FlushBuffer();
	}

	private interface IChannel:IDisposable{
		Error Subscribe(Receiver bce);
		Error Unsubscribe(int id);
		void UnsubscribeAll();
		void NextBroadcast(IFormatProvider provider, string message, params object[] args);
	}
/*
* ███████╗███╗   ██╗██╗   ██╗███╗   ███╗███████╗
* ██╔════╝████╗  ██║██║   ██║████╗ ████║██╔════╝
* █████╗  ██╔██╗ ██║██║   ██║██╔████╔██║███████╗
* ██╔══╝  ██║╚██╗██║██║   ██║██║╚██╔╝██║╚════██║
* ███████╗██║ ╚████║╚██████╔╝██║ ╚═╝ ██║███████║
* ╚══════╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝
*/
	/// <summary>
	/// Most of the methods return this enum to indicate possible errors.
	/// </summary>
	public enum Error{
		/// <summary>
		/// No error has occurred.
		/// </summary>
		NoError,
		/// <summary>
		/// Given Channel (at least one with the same id) has already been added to the network.
		/// </summary>
		ChannelAlreadyAdded,
		/// <summary>
		/// Given Receiver (at least one with the same id) has already been added to the network.
		/// </summary>
		ReceiverAlreadyAdded,
		/// <summary>
		/// Channel with given id has not been added before and is unknown to the network.
		/// </summary>
		NoSuchChannel,
		/// <summary>
		/// Receiver with given id has not been added before and is unknown to the network.
		/// </summary>
		NoSuchReceiver,
		/// <summary>
		/// Some argument which was passed to the method is null.
		/// </summary>
		ArgumentNull,
		/// <summary>
		/// The Network is not initialized yet and needs to be initialized first before executing the method.
		/// </summary>
		NetworkNotInitialized,
		/// <summary>
		/// The Network is already initialized.
		/// </summary>
		NetworkInitialized,
	}
	/// <summary>
	/// Option for Receiver if its queue is full and a new message gets pushed.
	/// </summary>
	public enum OnFullBufferDiscardOption{
		/// <summary>
		/// The message which would be the first one read on the next <see cref="NextMessage"/> call will be removed from the queue and is lost.
		/// </summary>
		DiscardOldestMessage,
		/// <summary>
		/// The newly pushed message will not be added to the queue and is lost.
		/// </summary>
		DiscardNewestMessage,
	}

	/// <summary>
	/// Option for Channel under which condition its default Receiver is active.
	/// </summary>
	public enum DefaultReceiverActivity{
		/// <summary>
		/// The default Receiver of the Channel will be active only if no other Receiver is subscribed to this Channel.
		/// </summary>
		OnlyActiveWhenNoneSubscribed,
		/// <summary>
		/// The default Receiver of the Channel will be always active.
		/// </summary>
		AlwaysActive,
		/// <summary>
		/// The default Receiver of the Channel will be never active.
		/// </summary>
		NeverActive,
	}


	private static Dictionary<int,Channel> channelFromId;
	private static Dictionary<int,Receiver> receiverFromId;

	private static bool isNotInitialized=true;

	/// <summary>
	/// Initialize the InfoNetwork before doing anything else.
	/// </summary>
	/// <returns>
	/// <see cref="Error.NetworkInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// </returns>
	public static Error Initialize(){
		if(!IN.isNotInitialized){return Error.NetworkInitialized;}
		IN.channelFromId=new Dictionary<int,Channel>();
		IN.receiverFromId=new Dictionary<int,Receiver>();
		IN.isNotInitialized=false;
		return Error.NoError;
	}

	/// <summary>
	/// Unsubscribe and clear everything.
	/// </summary>
	/// <remarks>
	/// Need to <see cref="Initialize"/> before the InfoNetwork can be used again. Call this to dispose of the internal IDisposables.<br/>
	/// ATTENTION: Not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// </returns>
	public static Error Unmake(){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		foreach(var entry in channelFromId){
			// Can only return NoError as network is initialized and all keys are contained.
			IN.UnsubscribeAllReceiversFromChannel(entry.Key);
			(entry.Value as IChannel).Dispose();
		}
		foreach(var entry in receiverFromId){
			(entry.Value as IReceiver).Dispose();
		}
		IN.channelFromId=null;
		IN.receiverFromId=null;
		IN.isNotInitialized=true;
		return Error.NoError;
	}

	/// <summary>
	/// Continue threads which are waiting for a broadcast.
	/// </summary>
	/// <remarks>
	/// Call this method after ensuring that no thread will reenter any <see cref="NextMessage"/>. After joining those threads changes to the InfoNetwork can be safely made.<br/>
	/// </remarks>
	public static void AbortAllWaits(){
		if(IN.isNotInitialized){return;}
		foreach(var receiverId in IN.receiverFromId.Keys){
			(IN.receiverFromId[receiverId] as IReceiver).AbortWaitOnEmpty();
		}
	}

	/// <summary>
	/// Continue thread of receiver if waiting for a broadcast.
	/// </summary>
	public static void AbortWait(this int receiverId){
		if(IN.isNotInitialized){return;}
		if(!IN.receiverFromId.ContainsKey(receiverId)){return;}
		(IN.receiverFromId[receiverId] as IReceiver).AbortWaitOnEmpty();
	}

	/// <summary>
	/// This extension can be used to check if a method did not execute successfully.
	/// </summary>
	/// <returns>
	/// <c>true</c> if <paramref name="err"/> is not equal <see cref="Error.NoError"/>. <c>false</c> otherwise.<br/>
	/// </returns>
	/// <param name="err">Some Error returned by a method.</param>
	public static bool Failed(this Error err){
		return err!=Error.NoError;
	}

	/// <summary>
	/// Aquire oldest message from queue of Receiver or optionally wait for a new message if the queue is empty.
	/// </summary>
	/// <remarks>
	/// This is the only method that is able to block a calling thread indefinitely (depending on configuration of the Receiver). Use <see cref="AbortAllWaits"/> to instantly wake all waiting threads.
	/// </remarks>
	/// <returns>
	/// <c>null</c> if:<br/>
	/// <list type="bullet">
	/// <item>Network is not initialized.</item>
	/// <item>Receiver with id equal to <paramref name="receiverId"/> is not part of network.</item>
	/// <item>Wait times out or is aborted.</item>
	/// </list>
	/// Otherwise the next message in queue.<br/>
	/// </returns>
	/// <param name="receiverId">Id of Receiver to check next message.</param>
	/// <param name="token">Optional cancellation token to abort the wait with.</param>
	public static string NextMessage(this int receiverId, CancellationToken token=default(CancellationToken)){
		if(IN.isNotInitialized){                       return null;}
		if(!IN.receiverFromId.ContainsKey(receiverId)){return null;}
		return (IN.receiverFromId[receiverId] as IReceiver).NextMessage(token);
	}

	/// <summary>
	/// Flush all queued messages.
	/// </summary>
	/// <param name="receiverId">Id of Receiver to flush.</param>
	public static void Flush(this int receiverId){
		if(IN.isNotInitialized){                       return;}
		if(!IN.receiverFromId.ContainsKey(receiverId)){  return;}
		(IN.receiverFromId[receiverId] as IReceiver).FlushBuffer();
	}

	/// <summary>
	/// Enqueue message to all subscribed <see cref="Receiver"/>s.
	/// </summary>
	/// <remarks>
	/// Can be used like String.Format.<br/>
	/// </remarks>
	/// <param name="channelId">Id of Channel to broadcast from.</param>
	/// <param name="message">Message to broadcast.</param>
	/// <param name="args">List of objects to insert into <paramref name="message"/> like String.Format.</param>
	public static void Broadcast(this int channelId, string message, params object[] args){
		if(IN.isNotInitialized){                       return;}
		if(!IN.channelFromId.ContainsKey(channelId)){  return;}
		(IN.channelFromId[channelId] as IChannel).NextBroadcast(null,message,args);
	}

	/// <summary>
	/// Enqueue message to all subscrined <see cref="Receiver"/>s.
	/// </summary>
	/// <remarks>
	/// Can be used like String.Format.<br/>
	/// </remarks>
	/// <param name="channelId">Id of Channel to broadcast from.</param>
	/// <param name="provider">Format provider to set e.g. the culture.</param>
	/// <param name="message">Message to broadcast.</param>
	/// <param name="args">List of objects to insert into <paramref name="message"/> like String.Format.</param>
	public static void Broadcast(this int channelId, IFormatProvider provider, string message, params object[] args){
		if(IN.isNotInitialized){                       return;}
		if(!IN.channelFromId.ContainsKey(channelId)){  return;}
		(IN.channelFromId[channelId] as IChannel).NextBroadcast(provider,message,args);
	}

/*
*  █████╗ ██████╗ ██████╗      ██████╗
* ██╔══██╗██╔══██╗██╔══██╗    ██╔════╝
* ███████║██║  ██║██║  ██║    ██║
* ██╔══██║██║  ██║██║  ██║    ██║
* ██║  ██║██████╔╝██████╔╝    ╚██████╗
* ╚═╝  ╚═╝╚═════╝ ╚═════╝      ╚═════╝
*/
	/// <summary>
	/// Add a Channel to the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// Channels must be added before they can be subscribed to by Receivers.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.ArgumentNull"/>: <paramref name="channel"/> is null.<br/>
	/// <see cref="Error.ChannelAlreadyAdded"/>: <paramref name="channel"/> with same Id already has been added to the InfoNetwork.<br/>
	/// </returns>
	/// <param name="channel">Channel to add to the network.</param>
	public static Error AddChannel(Channel channel){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		return IN.Internal_AddChannel(channel);
	}
	/// <summary>
	/// Add multiple Channels to the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// See <see cref="AddChannel"/>.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/>: Even if some <see cref="Channel"/>s could not be added. Use <see cref="AddChannel"/> to check errors individually.<br/>
	/// <see cref="Error.ArgumentNull"/>: <paramref name="channels"/> is null.<br/>
	/// </returns>
	/// <param name="channels"><see cref="Channel"/>s to add to the network.</param>
	public static Error AddChannels(Channel[] channels){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		if(channels==null){     return Error.ArgumentNull;}
		foreach(var channel in channels){
			IN.Internal_AddChannel(channel);
		}
		return Error.NoError;
	}
	private static Error Internal_AddChannel(Channel channel){
		if     (channel==null){                        return Error.ArgumentNull;}
		else if(channelFromId.ContainsKey(channel.Id)){return Error.ChannelAlreadyAdded;}
		else{   channelFromId[channel.Id]=channel;     return Error.NoError;}
	}

/*
*  █████╗ ██████╗ ██████╗     ██████╗
* ██╔══██╗██╔══██╗██╔══██╗    ██╔══██╗
* ███████║██║  ██║██║  ██║    ██████╔╝
* ██╔══██║██║  ██║██║  ██║    ██╔══██╗
* ██║  ██║██████╔╝██████╔╝    ██║  ██║
* ╚═╝  ╚═╝╚═════╝ ╚═════╝     ╚═╝  ╚═╝
*/
	/// <summary>
	/// Add a Receiver to the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// Receivers must be added before they can subscribe to Channels.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.ArgumentNull"/>: <paramref name="receiver"/> is null.<br/>
	/// <see cref="Error.ReceiverAlreadyAdded"/>: <paramref name="receiver"/> with same Id already has been added to the InfoNetwork.<br/>
	/// </returns>
	/// <param name="receiver">Receiver to add to the network.</param>
	public static Error AddReceiver(Receiver receiver){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		return IN.Internal_AddReceiver(receiver);
	}
	/// <summary>
	/// Add multiple Receivers to the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// See <see cref="AddReceiver"/>.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/>: Even if some <see cref="Receiver"/>s could not be added. Use <see cref="AddReceiver"/> to check errors individually.<br/>
	/// <see cref="Error.ArgumentNull"/>: <paramref name="receivers"/> is null.<br/>
	/// </returns>
	/// <param name="receivers">Receivers to add to the network.</param>
	public static Error AddReceivers(Receiver[] receivers){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		if(receivers==null){ return Error.ArgumentNull;}
		foreach(var receiver in receivers){
			IN.Internal_AddReceiver(receiver);
		}
		return Error.NoError;
	}
	private static Error Internal_AddReceiver(Receiver receiver){
		if     (receiver==null){                            return Error.ArgumentNull;}
		else if(receiverFromId.ContainsKey(receiver.Id)){return Error.ReceiverAlreadyAdded;}
		else{   receiverFromId[receiver.Id]=receiver; return Error.NoError;}
	}

/*
* ██████╗ ███████╗███╗   ███╗     ██████╗
* ██╔══██╗██╔════╝████╗ ████║    ██╔════╝
* ██████╔╝█████╗  ██╔████╔██║    ██║
* ██╔══██╗██╔══╝  ██║╚██╔╝██║    ██║
* ██║  ██║███████╗██║ ╚═╝ ██║    ╚██████╗
* ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝     ╚═════╝
*/
	/// <summary>
	/// Remove a Channel from the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.NoSuchChannel"/>: No Channel with id equal to <paramref name="channelId"/> was added to the network before.<br/>
	/// </returns>
	/// <param name="channelId">Id of Channel to remove from the network.</param>
	public static Error RemoveChannel(int channelId){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		return IN.Internal_RemoveChannel(channelId);
	}
	/// <summary>
	/// Remove multiple Channels from the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// See <see cref="RemoveChannel"/>.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/>: Even if some <see cref="Channel"/>s could not be removed. Use <see cref="RemoveChannel"/> to check errors individually.<br/>
	/// <see cref="Error.ArgumentNull"/>: <paramref name="channelIds"/> is null.<br/>
	/// </returns>
	/// <param name="channelIds">Channels to remove from the network.</param>
	public static Error RemoveChannels(int[] channelIds){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		if(channelIds==null){   return Error.ArgumentNull;}
		foreach(var channelId in channelIds){
			IN.Internal_RemoveChannel(channelId);
		}
		return Error.NoError;
	}
	private static Error Internal_RemoveChannel(int channelId){
		if(!IN.channelFromId.ContainsKey(channelId)){return Error.NoSuchChannel;}
		// Can only return NoError as network is initialized and all keys are contained.
		IN.UnsubscribeAllReceiversFromChannel(channelId);
		IN.channelFromId.Remove(channelId);
		return Error.NoError;
	}

/*
* ██████╗ ███████╗███╗   ███╗    ██████╗
* ██╔══██╗██╔════╝████╗ ████║    ██╔══██╗
* ██████╔╝█████╗  ██╔████╔██║    ██████╔╝
* ██╔══██╗██╔══╝  ██║╚██╔╝██║    ██╔══██╗
* ██║  ██║███████╗██║ ╚═╝ ██║    ██║  ██║
* ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝    ╚═╝  ╚═╝
*/
	/// <summary>
	/// Remove a Receiver from the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.NoSuchReceiver"/>: No Receiver with id equal to <paramref name="receiverId"/> was added to the network before.<br/>
	/// </returns>
	/// <param name="receiverId">Id of Receiver to remove from the network.</param>
	public static Error RemoveReceiver(int receiverId){
		if(IN.isNotInitialized){return Error.NetworkNotInitialized;}
		return IN.Internal_RemoveReceiver(receiverId);
	}
	/// <summary>
	/// Remove multiple Receivers from the InfoNetwork.
	/// </summary>
	/// <remarks>
	/// See <see cref="RemoveReceiver"/>.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/>: Even if some <see cref="Receiver"/>s could not be removed. Use <see cref="RemoveReceiver"/> to check errors individually.<br/>
	/// <see cref="Error.ArgumentNull"/>: <paramref name="receiverIds"/> is null.<br/>
	/// </returns>
	/// <param name="receiverIds">Receivers to remove from the network.</param>
	public static Error RemoveReceivers(int[] receiverIds){
		if(IN.isNotInitialized){ return Error.NetworkNotInitialized;}
		if(receiverIds==null){return Error.ArgumentNull;}
		foreach(var receiverId in receiverIds){
			IN.Internal_RemoveReceiver(receiverId);
		}
		return Error.NoError;
	}
	private static Error Internal_RemoveReceiver(int receiverId){
		if(!IN.receiverFromId.ContainsKey(receiverId)){return Error.NoSuchReceiver;}
		IN.UnsubscribeReceiverFromAllChannels(receiverId);
		IN.receiverFromId.Remove(receiverId);
		return Error.NoError;
	}

/*
* ███████╗██╗   ██╗██████╗
* ██╔════╝██║   ██║██╔══██╗
* ███████╗██║   ██║██████╔╝
* ╚════██║██║   ██║██╔══██╗
* ███████║╚██████╔╝██████╔╝
* ╚══════╝ ╚═════╝ ╚═════╝
*/

	/// <summary>
	/// Subscribe a Receiver to a Channel.
	/// </summary>
	/// <remarks>
	/// Channels and Receivers have to be added to the network to be connected/disconnected.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.NoSuchReceiver"/>: No Receiver with id equal to <paramref name="receiverId"/> was added to the network before.<br/>
	/// <see cref="Error.NoSuchChannel"/>: No Channel with id equal to <paramref name="channelId"/> was added to the network before.<br/>
	/// <see cref="Error.ReceiverAlreadyAdded"/>: Receiver with id equal to <paramref name="receiverId"/> is already subscribed to Channel with id equal to <paramref name="channelId"/>.<br/>
	/// </returns>
	/// <param name="receiverId">Id of Receiver to subscribe to Channel with id <paramref name="channelId"/>.</param>
	/// <param name="channelId">Id of Channel to subscribe Receiver with id <paramref name="receiverId"/> to.</param>
	public static Error SubscribeReceiverToChannel(int receiverId, int channelId){
		if(IN.isNotInitialized){                              return Error.NetworkNotInitialized;}
		if(!IN.channelFromId.ContainsKey(channelId)){         return Error.NoSuchChannel;}
		if(!IN.receiverFromId.ContainsKey(receiverId)){       return Error.NoSuchReceiver;}
		return (IN.channelFromId[channelId] as IChannel).Subscribe(IN.receiverFromId[receiverId]);

	}

	/// <summary>
	/// Unsubscribe a Receiver from a Channel.
	/// </summary>
	/// <remarks>
	/// Channels and Receivers have to be added to the network to be connected/disconnected.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.NoSuchReceiver"/>: No Receiver with id equal to <paramref name="receiverId"/> is subscribed to Channel with id equal to <paramref name="channelId"/>.<br/>
	/// <see cref="Error.NoSuchChannel"/>: No Channel with id equal to <paramref name="channelId"/> was added to the network before.<br/>
	/// </returns>
	/// <param name="receiverId">Id of Receiver to subscribe from Channel with id <paramref name="channelId"/>.</param>
	/// <param name="channelId">Id of Channel to unsubscribe Receiver with id <paramref name="receiverId"/> to.</param>
	public static Error UnsubscribeReceiverFromChannel(int receiverId, int channelId){
		if(IN.isNotInitialized){                              return Error.NetworkNotInitialized;}
		if(!IN.channelFromId.ContainsKey(channelId)){         return Error.NoSuchChannel;}
		return (IN.channelFromId[channelId] as IChannel).Unsubscribe(receiverId);
	}

	/// <summary>
	/// Unsubscribe a Receiver from all Channels.
	/// </summary>
	/// <remarks>
	/// Channels and Receivers have to be added to the network to be connected/disconnected.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/>: Even if some <see cref="Receiver"/>s could not be unsubscribed. Use <see cref="UnsubscribeReceiverFromChannel"/> to check errors individually.<br/>
	/// </returns>
	/// <param name="receiverId">Id of Receiver to unsubscribe from all Channels.</param>
	public static Error UnsubscribeReceiverFromAllChannels(int receiverId){
		if(IN.isNotInitialized){                             return Error.NetworkNotInitialized;}
		foreach(var channelId in IN.channelFromId.Keys){
			IN.UnsubscribeReceiverFromChannel(receiverId,channelId);
		}
		return Error.NoError;
	}

	/// <summary>
	/// Unsubscribe all Receivers from a Channel.
	/// </summary>
	/// <remarks>
	/// Channels and Receivers have to be added to the network to be connected/disconnected.<br/>
	/// ATTENTION: Manipulating the network is not thread safe. Call only after ensuring that no other threads use the InfoNetwork.<br/>
	/// </remarks>
	/// <returns>
	/// <see cref="Error.NetworkNotInitialized"/><br/>
	/// <see cref="Error.NoError"/><br/>
	/// <see cref="Error.NoSuchChannel"/>: No Channel with id equal to <paramref name="channelId"/> was added to the network before.<br/>
	/// </returns>
	/// <param name="channelId">Id of Channel to unsubscribe all Receivers from.</param>
	public static Error UnsubscribeAllReceiversFromChannel(int channelId){
		if(IN.isNotInitialized){                              return Error.NetworkNotInitialized;}
		if(!IN.channelFromId.ContainsKey(channelId)){         return Error.NoSuchChannel;}
		(IN.channelFromId[channelId] as IChannel).UnsubscribeAll();
		return Error.NoError;
	}

/*
* ██████╗ ███████╗ ██████╗███████╗██╗██╗   ██╗███████╗██████╗
* ██╔══██╗██╔════╝██╔════╝██╔════╝██║██║   ██║██╔════╝██╔══██╗
* ██████╔╝█████╗  ██║     █████╗  ██║██║   ██║█████╗  ██████╔╝
* ██╔══██╗██╔══╝  ██║     ██╔══╝  ██║╚██╗ ██╔╝██╔══╝  ██╔══██╗
* ██║  ██║███████╗╚██████╗███████╗██║ ╚████╔╝ ███████╗██║  ██║
* ╚═╝  ╚═╝╚══════╝ ╚═════╝╚══════╝╚═╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝
*/

	/// <summary>
	/// <see cref="Receiver"/>s can be subcribed to <see cref="Channel"/>s.
	/// </summary>
	/// <remarks>
	/// Generate a Receiver, add it to the network and subscribe it to all <see cref="Channel"/>s needed. The behaviour of the Receiver can be configured with its properties<br/>
	/// </remarks>
	public class Receiver:IReceiver{

		/// <summary>
		/// Default buffer size of Receiver.
		/// </summary>
		public const int                       DEFAULT_DEFAULTRECEIVERBUFFERSIZE=100;
		/// <summary>
		/// Default OnFullBufferDiscardOption of Receiver.
		/// </summary>
		public const OnFullBufferDiscardOption DEFAULT_ONFULLBUFFERDISCARDING=OnFullBufferDiscardOption.DiscardOldestMessage;
		/// <summary>
		/// Default waiting behaviour of Receiver.
		/// </summary>
		public const bool                      DEFAULT_WAITONEMPTY=           true;
		/// <summary>
		/// Default timeout of Receiver.
		/// </summary>
		public const int                       DEFAULT_TIMEOUT=               -1;
		/// <summary>
		/// Default name-prepending behaviour of Receiver.
		/// </summary>
		public const bool                      DEFAULT_PREPENDNAME=           false;


		/// <summary>
		/// Id of Receiver. <see cref="Receiver"/>s will be discriminated only by their id.
		/// </summary>
		public readonly int           Id;
		/// <summary>
		/// Optional name for Receiver. Will be set to <see cref="Receiver.Id"/> if not given.
		/// </summary>
		public readonly string        Name;
		/// <summary>
		/// Size of queue of Receiver. Defaults to <see cref="Receiver.DEFAULT_DEFAULTRECEIVERBUFFERSIZE"/>.
		/// </summary>
		public readonly int           BufferSize;

		private Queue<string>         buffer;
		private ManualResetEventSlim  signalBufferNotEmpty;
		private SemaphoreSlim         bufferSemaphore;
		private bool                  isDisposed;


		/// <summary>
		/// Configure which message should be discarded when the queue is full and a new message arrives. Defaults to <see cref="Receiver.DEFAULT_ONFULLBUFFERDISCARDING"/>.
		/// </summary>
		public OnFullBufferDiscardOption OnFullBufferDiscardOption{get;set;}
		/// <summary>
		/// When <c>false</c> Receiver behaves as if <see cref="Receiver.Timeout"/> has been set to <c>0</c> when aquiring message. Else <see cref="Receiver.Timeout"/> will be applied. Defaults to <see cref="Receiver.DEFAULT_WAITONEMPTY"/>.
		/// </summary>
		public bool                      WaitOnEmpty{get;set;}
		/// <summary>
		/// When <c>true</c> <see cref="Receiver.Name"/> will be prepended before every message. Defaults to <see cref="Receiver.DEFAULT_PREPENDNAME"/>.
		/// </summary>
		public bool                      PrependName{get;set;}
		/// <summary>
		/// Configure how long to wait for a new message when the queue is empty in milliseconds. Zero indicates a non blocking read. Negative numbers indicate an indefinite wait without timeout. Defaults to <see cref="Receiver.DEFAULT_TIMEOUT"/>.
		/// </summary>
		public int                       Timeout{get{return this.timeout;}set{this.timeout=value<-1?-1:value;}}private int timeout;


		/// <summary>
		/// Initialize a new Receiver.
		/// </summary>
		public Receiver(int id, string name=null, int bufferSize=DEFAULT_DEFAULTRECEIVERBUFFERSIZE){
			this.Id=                       id;
			this.Name=                     name??this.Id.ToString();
			this.BufferSize=               bufferSize;
			this.buffer=                   new Queue<string>(bufferSize);
			this.signalBufferNotEmpty=     new ManualResetEventSlim(false);
			this.bufferSemaphore=          new SemaphoreSlim(1,1);
			this.OnFullBufferDiscardOption=DEFAULT_ONFULLBUFFERDISCARDING;
			this.WaitOnEmpty=              DEFAULT_WAITONEMPTY;
			this.Timeout=                  DEFAULT_TIMEOUT;
			this.PrependName=              DEFAULT_PREPENDNAME;
			this.isDisposed=               false;
		}

		void IDisposable.Dispose(){
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Can only be accessed through <see cref="InfoNetwork.Unmake"/>.
		/// </summary>
		protected virtual void Dispose(bool disposing){
			if(this.isDisposed){return;}
			if(disposing){
				this.signalBufferNotEmpty.Dispose();
				this.bufferSemaphore.Dispose();
			}
			this.isDisposed=true;
		}

		void IReceiver.AbortWaitOnEmpty(){
			this.signalBufferNotEmpty.Set();
		}

		string IReceiver.NextMessage(CancellationToken token){
			string message=this.ReadFromBuffer(this.WaitOnEmpty?this.Timeout:0,token);
			if(message==null){
				return null;
			}else{
				return (this.PrependName?this.Name+": ":String.Empty)+message;
			}
		}

		void IReceiver.WriteToBuffer(string message){
			this.WriteToBuffer(message);
		}

		void IReceiver.FlushBuffer(){
			/*LOCK BUFFER*/try{this.bufferSemaphore.Wait();
			this.buffer.Clear();
			/*RELASE BUFFER*/}finally{this.bufferSemaphore.Release();}
		}

		private void WriteToBuffer(string message){
			/*LOCK BUFFER*/try{this.bufferSemaphore.Wait();
			bool bufferFull=this.buffer.Count>=this.BufferSize;
			bool discardOldest=this.OnFullBufferDiscardOption==OnFullBufferDiscardOption.DiscardOldestMessage;
			// If there is no space and we are allowed to make some by discarding.
			// -> Discard first in queue.
			if(bufferFull && discardOldest){
				this.buffer.Dequeue();
			}
			// If there is space or we just made some space by discarding.
			// -> Add message to end of queue.
			if(!bufferFull || discardOldest){
				this.buffer.Enqueue(message);
			}
			if(this.buffer.Count==1){
				this.signalBufferNotEmpty.Set();
			}
			/*RELASE BUFFER*/}finally{this.bufferSemaphore.Release();}
		}

		private string ReadFromBuffer(int millisecondsTimeout, CancellationToken token){
			string message=null;
			/*LOCK BUFFER*/try{this.bufferSemaphore.Wait();
			if(this.buffer.Count==0){
				this.signalBufferNotEmpty.Reset();
			}
			/*RELASE BUFFER*/}finally{this.bufferSemaphore.Release();}
			if(!this.signalBufferNotEmpty.Wait(millisecondsTimeout,token)){return null;}
			/*LOCK BUFFER*/try{this.bufferSemaphore.Wait();
			if(this.buffer.Count>0){
				message=this.buffer.Dequeue();
			}
			/*RELASE BUFFER*/}finally{this.bufferSemaphore.Release();}
			return message;
		}
	}// Receiver

/*
*  ██████╗██╗  ██╗ █████╗ ███╗   ██╗███╗   ██╗███████╗██╗
* ██╔════╝██║  ██║██╔══██╗████╗  ██║████╗  ██║██╔════╝██║
* ██║     ███████║███████║██╔██╗ ██║██╔██╗ ██║█████╗  ██║
* ██║     ██╔══██║██╔══██║██║╚██╗██║██║╚██╗██║██╔══╝  ██║
* ╚██████╗██║  ██║██║  ██║██║ ╚████║██║ ╚████║███████╗███████╗
*  ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚══════╝╚══════╝
*/

	/// <summary>
	/// <see cref="Channel"/>s can be subscribed to by <see cref="Receiver"/>s.
	/// </summary>
	public partial class Channel:IChannel{

		/// <summary>
		/// Default name-prepending behaviour of Channel.
		/// </summary>
		public const bool DEFAULT_PREPENDNAME=false;

		/// <summary>
		/// Id of Channel. <see cref="Channel"/>s will be discriminated only by their id.
		/// </summary>
		public readonly int    Id;
		/// <summary>
		/// Optional name for Channel. Will be set to <see cref="Channel.Id"/> if not given.
		/// </summary>
		public readonly string Name;

		private Dictionary<int,Receiver> receiverFromId;
		private SemaphoreSlim            receiverFromIdLock;
		private bool                     isDisposed;

		/// <summary>
		/// When <c>true</c> <see cref="Channel.Name"/> will be prepended before every message. Defaults to <see cref="Channel.DEFAULT_PREPENDNAME"/>.
		/// </summary>
		public bool PrependName{get;set;}



		/// <summary>
		/// Initialize a new Channel.
		/// </summary>
		public Channel(int id, string name=null){
			this.Id=                          id;
			this.Name=                        name??this.Id.ToString();
			this.PrependName=                 DEFAULT_PREPENDNAME;
			this.receiverFromIdLock=       new SemaphoreSlim(1,1);
			this.isDisposed=                  false;
		}

		void IDisposable.Dispose(){
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Can only be accessed through <see cref="InfoNetwork.Unmake"/>.
		/// </summary>
		protected virtual void Dispose(bool disposing){
			if(this.isDisposed){return;}
			if(disposing){
				this.receiverFromIdLock.Dispose();
			}
			this.isDisposed=true;
		}

		Error IChannel.Subscribe(Receiver bce){
			if(bce==null){return Error.ArgumentNull;}
			/*LOCK receiverFromId*/try{this.receiverFromIdLock.Wait();
			if(this.receiverFromId==null || this.receiverFromId.Count==0){
				this.receiverFromId=new Dictionary<int,Receiver>();
			}else if(this.receiverFromId.ContainsKey(bce.Id)){
				return Error.ReceiverAlreadyAdded;
			}
			this.receiverFromId[bce.Id]=bce;
			/*RELASE receiverFromId*/}finally{this.receiverFromIdLock.Release();}
			return Error.NoError;
		}


		void IChannel.UnsubscribeAll(){
			/*LOCK receiverFromId*/try{this.receiverFromIdLock.Wait();
			if(this.receiverFromId!=null){
				this.receiverFromId=null;
			}
			/*RELASE receiverFromId*/}finally{this.receiverFromIdLock.Release();}
		}

		Error IChannel.Unsubscribe(int id){
			Error err;
			/*LOCK receiverFromId*/try{this.receiverFromIdLock.Wait();
			if(this.receiverFromId!=null && this.receiverFromId.ContainsKey(id)){
				this.receiverFromId.Remove(id);
				if(this.receiverFromId.Count==0){
					this.receiverFromId=null;
				}
				err=Error.NoError;
			}else{
				err=Error.NoSuchReceiver;
			}
			/*RELASE receiverFromId*/}finally{this.receiverFromIdLock.Release();}
			return err;
		}


		void IChannel.NextBroadcast(IFormatProvider provider, string message, params object[] args){
			string formattedMessage;
			try{
				if(provider==null){
					formattedMessage=String.Format(message,args);
				}else{
					formattedMessage=String.Format(provider,message,args);
				}
			}catch(FormatException){
				formattedMessage=message;
			}
			if(this.PrependName){
				formattedMessage=this.Name+": "+formattedMessage;
			}
			/*LOCK receiverFromId*/try{this.receiverFromIdLock.Wait();
			if(this.receiverFromId!=null){
				foreach(var entry in this.receiverFromId){
					(entry.Value as IReceiver).WriteToBuffer(formattedMessage);
				}
			}
			/*RELASE receiverFromId*/}finally{this.receiverFromIdLock.Release();}
		}

	}
}// class InfoNetwork
}// namespace Geraet

