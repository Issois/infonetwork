
using System;
using System.Threading;

namespace Geraet{

	internal static class ConsoleEntryPoint{

		public static void Main(string[] args){
			System.Console.WriteLine("Start");

			return;
			InfoNetwork.Initialize();

			var c1=new InfoNetwork.Channel(10);
			var c2=new InfoNetwork.Channel(20);
			var c3=new InfoNetwork.Channel(30);
			var r1=new InfoNetwork.Receiver(1);
			var r2=new InfoNetwork.Receiver(2);
			var r3=new InfoNetwork.Receiver(3);
			r1.PrependName=true;
			r2.PrependName=true;
			r3.PrependName=true;
			c1.PrependName=true;
			c2.PrependName=true;
			c3.PrependName=true;

			InfoNetwork.AddChannels(new InfoNetwork.Channel[]{c1,c2,c3});
			InfoNetwork.AddReceivers(new InfoNetwork.Receiver[]{r1,r2,r3});

			InfoNetwork.SubscribeReceiverToChannel(1,10);
			InfoNetwork.SubscribeReceiverToChannel(2,10);
			InfoNetwork.SubscribeReceiverToChannel(2,20);
			InfoNetwork.SubscribeReceiverToChannel(3,30);

			Thread r1Thread=new Thread(Read1);
			Thread r2Thread=new Thread(Read2);
			Thread r3Thread=new Thread(Read3);
			r1Thread.Start();
			r2Thread.Start();
			r3Thread.Start();
			Thread.Sleep(1000);
			System.Console.WriteLine("----");
			10.Broadcast("lol");
			Thread.Sleep(500);
			System.Console.WriteLine("----");
			20.Broadcast("xd");
			30.Broadcast("nice");
			Thread.Sleep(1000);
			System.Console.WriteLine("----");
			30.Broadcast("nice2");
			10.Broadcast("lol2");
			30.Broadcast("nice3");
			Thread.Sleep(500);
			System.Console.WriteLine("----");
			30.Broadcast("nice4");
			20.Broadcast("xd2");
			Thread.Sleep(1000);
			System.Console.WriteLine("----");
			Read1_keepListening=false;
			Read2_keepListening=false;
			Read3_keepListening=false;
			InfoNetwork.AbortAllWaits();
			r1Thread.Join();
			r2Thread.Join();
			r3Thread.Join();
			InfoNetwork.Unmake();
			// c1.SendToBroadcastees("Spam");
			// 3.Broadcast();
			// var chn=new InfoNetwork.Channel();
			// chn.Broadcast("1:<>");
			// chn.Broadcast("2:<{0}>",1234);
			// chn.Broadcast("3:<{0},{1}>",false,331234);
			// chn.Broadcast("4:<>",false,331234);
			// chn.Broadcast("5:<{0},{1}>",false);
			// chn.Broadcast("6:<{0},{1}>");
		}

		static bool Read1_keepListening=true;
		private static void Read1(){
			string msg;
			while(Read1_keepListening){
				msg=1.NextMessage();
				if(msg==null){continue;}
				System.Console.WriteLine("R1: "+msg);
			}
		}

		static bool Read2_keepListening=true;
		private static void Read2(){
			string msg;
			while(Read2_keepListening){
				msg=2.NextMessage();
				if(msg==null){continue;}
				System.Console.WriteLine("R2: "+msg);
			}
		}

		static bool Read3_keepListening=true;
		private static void Read3(){
			string msg;
			while(Read3_keepListening){
				msg=3.NextMessage();
				if(msg==null){continue;}
				System.Console.WriteLine("R3: "+msg);
			}
		}
	}
}