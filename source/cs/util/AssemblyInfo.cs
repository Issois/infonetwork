﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Geraet.InfoNetwork")]
[assembly: AssemblyDescription("Manage multithreaded message propagation")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Geraet")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.2.2.12")]
