@echo off
echo "  ____  _    _ _____ _      _____  "&
echo " |  _ \| |  | |_   _| |    |  __ \ "&
echo " | |_) | |  | | | | | |    | |  | |"&
echo " |  _ <| |  | | | | | |    | |  | |"&
echo " | |_) | |__| |_| |_| |____| |__| |"&
echo " |____/ \____/|_____|______|_____/ "&
echo "                                   "&
echo "                                   "

set csprojFile=project.csproj
set compilerName=MSBuild.exe
set compiler472location="C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\%compilerName%"
set compiler403location="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\%compilerName%"

set CouldNotFindCommandErrorLevel=9009
set CouldNotFindPathErrorLevel=3
set MSBuildFoundErrorLevel=1

rem Test, which of the compilers can be found.
rem If multiple will be found, the lowest one has priority.
%compiler403location% " " > nul 2>&1
if "%errorlevel%"=="%MSBuildFoundErrorLevel%" (set compiler=%compiler403location%)
%compiler472location% " " > nul 2>&1
if "%errorlevel%"=="%MSBuildFoundErrorLevel%" (set compiler=%compiler472location%)
%compilerName% " " > nul 2>&1
if "%errorlevel%"=="%MSBuildFoundErrorLevel%" (set compiler=%compilerName%)

echo Using compiler: %compiler%

%compiler% %~dp0%csprojFile% /p:parameters=%1