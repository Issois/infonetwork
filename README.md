# InfoNetwork

c# library for message propagation.

### Note: Multithreading
The network is only threadsafe between Broadcast and NextMessage. Do not modifiy the network on one thread while other threads are broadcasting or receiving messages.

### Usage
First initialize the network:
```cs
InfoNetwork.Initialize();
```
Initialize some channels and receivers and configure them to your liking:
```cs
int c0=0,c1=1,c2=2,r0=0,r1=1,r2=2;
var tmp_c0=new InfoNetwork.Channel(c0,"my first channel");
var tmp_c1=new InfoNetwork.Channel(c1,"my second channel",2000);
var tmp_c2=new InfoNetwork.Channel(c2);
var tmp_r0=new InfoNetwork.Reciever(r0,"small receiver",5);
tmp_c0.PrependName=true;
tmp_c1.DefaultReceiverActivity=InfoNetwork.DefaultReceiverActivity.NeverActive;
tmp_c2.DefaultReceiverActivity=InfoNetwork.DefaultReceiverActivity.AlwaysActive;
tmp_r0.WaitOnEmpty=false;
```
Add them to the network:
```cs
InfoNetwork.AddChannel(c0);
InfoNetwork.AddChannels(new InfoNetwork.Channel[]{
	tmp_c1,
	tmp_c2,
});
InfoNetwork.AddReceivers(new InfoNetwork.Reciever[]{
	tmp_r0,
	// This is the most concise way for initializing and adding:
	new InfoNetwork.Reciever(r1){Timeout=1000,PrependName=true},
	new InfoNetwork.Reciever(r2)
});
```
Subscribe receivers to channels:
```cs
InfoNetwork.SubscribeReceiverToChannel(r1,c0);
InfoNetwork.SubscribeReceiverToChannel(r0,c0);
InfoNetwork.SubscribeReceiverToChannel(r0,c2);
```
Broadcast and and receive messages (from different threads):
```cs
c0.Broadcast("important message");
c1.Broadcast("No one cares"); // Nothing happens here.
c2.Broadcast("another message");
c2.NextMessage(); // Returns "another message";
r0.NextMessage(); // Returns "important message" or "another message";
r1.NextMessage(); // Returns "important message";
r0.NextMessage(); // Returns "another message" or "important message";
r0.NextMessage(); // Returns null;
r2.NextMessage(); // Waits for a broadcast;
```
### Cleanup
If threads are still waiting for messages first make sure they will not reenter the wait and then abort the wait with:
```cs
InfoNetwork.AbortAllWaits();
```
Now all threads can be joined. Now you can safely modifiy the newtwork (adding, removing, subscribing, unsubscribing) or dispose everything with:
```cs
InfoNetwork.Unmake();
```